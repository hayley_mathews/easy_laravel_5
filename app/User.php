<?php

namespace todoparrot;

use Illuminate\Foundation\Auth\User as Authenticatable;
use todoparrot\Todolist;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = $password;
    }

    public function profile()
    {
        return $this->hasOne('todoparrot\Profile');
    }

    public function lists()
    {
        return $this->hasMany('todoparrot\Todolist');
    }

    public function owns($listId)
    {
        $list = Todolist::find($listId);

        if ($list->user_id == $this->id)
        {
            return true;
        } else {
            return false;
        }

    }
}
