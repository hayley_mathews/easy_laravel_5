<?php

namespace todoparrot\Http\Controllers;

use Illuminate\Http\Request;

use todoparrot\Http\Requests;
use todoparrot\Todolist;
use todoparrot\User;
use todoparrot\Task;
use todoparrot\Http\Requests\TaskCreateFormRequest;

class TasksController extends Controller
{

    /**
     * Make sure the user is authenticated
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @param $listId
     * @return $this
     */
    public function create($listId)
    {
        $user = User::find(\Auth::id());

        if ($user->owns($listId)){
            $list = Todolist::find($listId);
            return view('lists.tasks.create')->with('list', $list);
        } else {

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($listId, TaskCreateFormRequest $request)
    {
        $user = User::find(\Auth::id());

        if ($user->owns($listId)) {
            $list = Todolist::find($listId);

            $task = new Task(array(
                'name' => $request->get('name'),
                'done' => true ? $request->get('done') == 'true' : false
            ));

            $task = $list->tasks()->save($task);

            return \Redirect::route('lists.show', array($list->id))->with('message', 'Your task has been created');
        } else {
            return \Redirect::route('home')->with('message', 'Authorization error: you do not own this list.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($listId, $taskId)
    {
        $user = User::find(\Auth::id());

        $task = Task::find($taskId);

        if ($user->owns($task->todolist->id)){
            return view('tasks.edit')->with('task', $task);
        } else {
            return \Redirect::route('lists.index')->with('message', 'Permission error!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($listId, $taskId, TaskCreateFormRequest $request)
    {
        $user = User::find(\Auth::id());

        $task = Task::find($taskId);

        if ($user->owns($task->todolist->id)){
            $task->update([
                'name' => $request->get('name'),
                'done' => true ? $request->get('done') == 'true' : false
            ]);

            return \Redirect::route('lists.tasks.edit', array($task->todolist->id))->with('message', 'Your task has been updated');
        } else {
            return \Redirect::route('lists.index')->with('message', 'Permissions error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($listId, $taskId)
    {
        $user = \Auth::user();
        $task = Task::findOrFail($taskId);
        $list = Todolist::findOrFail($task->todolist->id);
        if ($user->owns($list->id)) {
            $task->delete();
            return \Redirect::route('lists.show', [$list->id])
                ->with('message', 'Task deleted!');
        }
    }

    public function complete($listId, $taskId)
    {
        $user = User::find(\Auth::id());

        $list = Todolist::find($listId);

        if ($user->owns($listId)) {
            $task = $list->tasks()->where('id', '=', $taskId)->first();

            if ($task->done == true){
                $task->done = false;
            } else {
                $task->done = true;
            }

            $task->save();

            return \Redirect::route('lists.show', [$list->id])->with('message', 'Task updated');
        } else {
            return \Redirect::route('home')->with('message', 'Permissions error');
        }
    }
}
