<?php

namespace todoparrot\Http\Controllers;

use todoparrot\Http\Requests;
use Illuminate\Http\Request;
use todoparrot\Todolist;
use todoparrot\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = User::find(\Auth::id())->lists();
        return view('home')->with('lists', $lists);
    }
}
