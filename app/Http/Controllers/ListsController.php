<?php

namespace todoparrot\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use todoparrot\Http\Requests;
use todoparrot\Todolist;
use todoparrot\Http\Requests\ListFormRequest;
use todoparrot\Category;
use todoparrot\User;

class ListsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('listowner', ['only' => ['show', 'edit', 'update']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = User::find(\Auth::id())->lists()->orderBy('created_at', 'desc')->paginate(15);
        return view('lists.index')->with('lists', $lists);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::lists('name', 'id');

        return view('lists.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ListFormRequest $request)
    {
        $list = new Todolist(array(
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ));

        $user = User::find(\Auth::id());
        $list = $user->lists()->save($list);


        return \Redirect::route('lists.index')->with('message', 'Your list has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list = Todolist::find($id);
        return view('lists.show')->with('list', $list);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = Todolist::find($id);

        return view('lists.edit')->with('list', $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, ListFormRequest $request)
    {
        $user = \Auth::user();

        $list = Todolist::find($id);

        $list->update([
            'name' => $request->get('name'),
            'description' => $request->get('description')]);

        return \Redirect::route('lists.edit', array($list->id))->with('message', 'Your list has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Todolist::destroy($id);
        return \Redirect::route('lists.index')->with('message', 'The list has been deleted');
    }
}
