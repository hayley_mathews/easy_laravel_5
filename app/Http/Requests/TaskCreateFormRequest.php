<?php namespace todoparrot\Http\Requests;

use todoparrot\Http\Requests\Request;

class TaskCreateFormRequest extends Request {
    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
    public function authorize()
    {
        return true;
    }
}