<?php

namespace todoparrot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Todolist extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    private $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

    public function validate()
    {
        $v = \Validator::make($this->attributes, $this->rules);
        if ($v->passes()) return true;
        $this->errors = $v->messages();
        return false;
    }

    public function user()
    {
        return $this->belongsTo('todoparrot\User');
    }

    public function tasks()
    {
        return $this->hasMany('todoparrot\Task');
    }

    public function categories()
    {
        return $this->belongsToMany('todoparrot\Category')->withTimestamps();
    }

    public function comments()
    {
        return $this->morphMany('\todoparrot\Comment', 'commentable');
    }
}
