<?php

namespace todoparrot;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $fillable = [
        'name', 'done'
    ];

    public function todolist()
    {
        return $this->belongsTo('todoparrot\Todolist');
    }

    public function scopeDone($query, $flag)
    {
        return $query->where('done', $flag);
    }
}
