@extends('layouts.app')

@section('content')

    {!! Form::model($list, array('method' => 'put', 'route' => ['lists.update', $list->id], 'class' => 'form')) !!}

    <div class ="form-group">
        {!! Form::label('List Name') !!}
        {!! Form::text('name', null, array('required', 'class' => 'form-control', 'placeholder' => 'List name')) !!}
    </div>

    <div class ="form-group">
        {!! Form::label('List Description') !!}
        {!! Form::textarea('description', null, array('required', 'class' => 'form-control', 'placeholder' => 'Your description')) !!}
    </div>

    <div class ="form-group">
        {!! Form::submit('Update List', array('class'=> 'btn btn-primary')) !!}
    </div>

    {!! Form::close() !!}


@endsection