@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Lists</div>

                <div class="panel-body">
                    @if ($lists->count() > 0)
                        <ul>
                            @foreach ($lists as $list)
                                <li><a href="{{ url('/lists/' . $list->id) }}">{{ $list->name }} : {{$list->description}}</a></li>
                            @endforeach
                            {!! $lists->render() !!}
                        </ul>
                    @else
                        <p>
                            You don't have any lists. <br />
                            <a  href="{{ url('/lists/create') }}"
                               class = "btn btn-primary">Make one</a>

                        </p>
                    @endif
                </div>
            </div>
        </div>



@endsection