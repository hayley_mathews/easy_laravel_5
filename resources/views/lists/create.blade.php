@extends('layouts.app')

@section('content')

    <div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create a New List</div>

                <div class="panel-body">

                    {!! Form::open(array('route' => 'lists.store', 'class' => 'form')) !!}
                    <div class ="form-group">
                        {!! Form::label('List Name') !!}
                        {!! Form::text('name', null, array('required', 'class' => 'form-control', 'placeholder' => 'San Juan Vacation')) !!}
                    </div>

                    <div class ="form-group">
                        {!! Form::label('List Description') !!}
                        {!! Form::textarea('description', null, array('required', 'class' => 'form-control', 'placeholder' => 'things to do before trip to San Juan')) !!}
                    </div>

                    <div class ="form-group">
                        {!! Form::submit('Create List', array('class'=> 'btn btn-primary')) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    </div>



@endsection