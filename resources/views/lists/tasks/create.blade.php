@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create a New Task for {{$list->name}}</div>

                    <div class="panel-body">

                        {!! Form::open(array('route' => array('lists.tasks.store', $list->id), 'class' => 'form')) !!}
                        <div class ="form-group">
                            {!! Form::label('Task Name') !!}
                            {!! Form::text('name', null, array('required', 'class' => 'form-control', 'placeholder' => 'Task name')) !!}
                        </div>

                        <div class ="form-group">
                            {!! Form::submit('Create Task', array('class'=> 'btn btn-primary')) !!}
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection