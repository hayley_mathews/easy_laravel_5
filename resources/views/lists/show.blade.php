@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>{{$list->name}}</h3></div>

                    <div class="panel-body">
                        <h4>Description</h4>
                        <p>
                            {{ $list->description }}
                        </p>

                        <h4>Tasks</h4>
                        @if( $list->tasks->count() > 0)
                            <ul>
                                @foreach($list->tasks as $task)
                                    <li> {{ $task->name  }}</li>
                                @endforeach
                            </ul>
                            <p>
                                <a href="{{ URL::route('lists.tasks.create', [$list->id]) }}"
                                   class = "btn btn-primary">Add a new task</a>
                            </p>
                        @else
                            <p>
                                You haven't created any tasks. <br />
                                <a href="{{ URL::route('lists.tasks.create', [$list->id]) }}"
                                   class = "btn btn-primary">Create a task</a>
                            </p>
                        @endif
                        @foreach($list->comments as $comment)
                            <p>
                                {{$comment->body}}
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection