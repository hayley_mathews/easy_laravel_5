@extends('layouts.master')

@section('content')

    <h1>Contact TODOParrot</h1>


    {!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}
    <div class ="form-group">
        {!! Form::label('Your Name') !!}
        {!! Form::text('name', null, array('required', 'class' => 'form-control', 'placeholder' => 'Your name')) !!}
    </div>

    <div class ="form-group">
        {!! Form::label('Your Email Address') !!}
        {!! Form::text('email', null, array('required', 'class' => 'form-control', 'placeholder' => 'Your email address')) !!}
    </div>

    <div class ="form-group">
        {!! Form::label('Your Message') !!}
        {!! Form::textarea('message', null, array('required', 'class' => 'form-control', 'placeholder' => 'Your message')) !!}
    </div>

    <div class ="form-group">
        {!! Form::submit('Contact Us!', array('class'=> 'btn btn-primary')) !!}
    </div>
    {!! Form::close() !!}

@endsection