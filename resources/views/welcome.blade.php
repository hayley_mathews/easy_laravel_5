@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12" style="text-align: center; padding-top: 10%;">
            <h1>Welcome to ListsListsLists</h1>
            <p>
                We have lists and shit. Mostly just lists though.
            </p>

            @if (! Auth::check())
                <a href="/auth/register" class="btn btn-primary">Sign Up</a>
            @else
                <a href="{{ URL::route('lists.index') }}" class="btn btn-primary">View Your Lists</a>
            @endif

            <p id="list_count">

            </p>

        </div>
    </div>

@endsection
